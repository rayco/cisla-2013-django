from django.contrib import admin
from models import Post, Comment

class CommentInline(admin.StackedInline):
    model = Comment
    extra = 1
    can_delete = 0

class PostAdmin(admin.ModelAdmin):
    #fields = ('titulo','intro', 'mas')                 # Campos visibles desde la interfaz administrativa
    list_display = ('id', 'titulo', 'visto')            # Columnas que aparecen en la tabla
    #list_display = ('__unicode__', 'titulo', 'cuenta') # Otra forma para mostrar las columnas

    inlines = [
        CommentInline,
    ]                                                   # Asociar los comentarios de un Post para poder modificarlos

admin.site.register(Post, PostAdmin)
admin.site.register(Comment)

