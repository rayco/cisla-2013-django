from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.
class Post(models.Model):
    titulo = models.CharField(max_length=200)
    intro = models.TextField()
    mas = models.TextField(null=True, blank=True)
    visto = models.IntegerField(default=0)
    publicado = models.BooleanField(default=False)

    def __unicode__(self):
        return u"%s - %s" % (self.id, self.titulo)

    def cuenta(self):
        return "%s" % self.visto
    cuenta.short_discription = "Numero"

    def get_absolute_url(self):
        return reverse("post_detail", kwargs={'pk' : self.pk})

class Comment(models.Model):
    text = models.TextField()
    post = models.ForeignKey('Post')
    #post = models.ForeignKey('Post', related_name="comentarios")   # related_name es necesario cuando tenemos varias relaciones
                                                                    # Importante para poder llamar a comment_set.all()

