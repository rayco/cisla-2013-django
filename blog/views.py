# Create your views here.
from models import Post
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from forms import PostForm

from rest_framework import generics
from serializers import PostSerializer

#def post_list(request):
#    posts = Post.objects.all()
#   context = {'posts' : posts}
#   return render_to_response("blog/list.html", context)
#
#def post_detail(request, pk):
#    post = get_object_or_404(Post, pk=pk)
#
#    #get_object_or_404 es equivalente a:
#    #try:
#    #    post = Post.objects.get(pk = pk)
#    #except: Post.DoesNotExist:
#    #    raise Http404
#    
#    context = {'post' : post}
#    return render_to_response("blog/detail.html", context)

# LO MISMO PERO CON VISTAS GENERICAS

class PostList(ListView):
    model = Post

class PostDetail(DetailView):
    model = Post

class PostCreate(CreateView):
    model = Post
    form_class = PostForm

class PostUpdate(UpdateView):
    model = Post
    form_class = PostForm

class PostDelete(DeleteView):
    model = Post
    success_url = reverse_lazy('post_list')


class PostListREST(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class PostDetailREST(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class =  PostSerializer

