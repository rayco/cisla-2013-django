from django import forms
from models import Post
from tinymce.widgets import TinyMCE

class PostForm(forms.ModelForm):
    mas = forms.CharField(
            widget=TinyMCE(),
            required = False,
            )

    class Meta:
        model = Post
