from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from blog.views import PostList, PostDetail, PostCreate, PostUpdate, PostDelete, PostListREST, PostDetailREST
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'amazing_blog.views.home', name='home'),
    # url(r'^amazing_blog/', include('amazing_blog.foo.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^post/create/$', login_required(PostCreate.as_view()), name="post_create"),
    url(r'^post/(?P<pk>\d+)/edit$', login_required(PostUpdate.as_view()), name="post_update"),
    url(r'^post/(?P<pk>\d+)/delete$', login_required(PostDelete.as_view()), name="post_delete"),
    url(r'^post/(?P<pk>\d+)/$', PostDetail.as_view(), name="post_detail"),
    url(r'^$', PostList.as_view(), name="post_list"),
    url(r'', include('social_auth.urls')),
    url(r'^logout/$', 'django.contrib.auth.views.logout', { 'next_page': '/' }, name='logout'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #url(r'post/(?P<pk>\d+)/$', "blog.views.post_detail", name="post_detail"),
    #url(r'^$', "blog.views.post_list", name="post_list"),
    url(r'^posts/$', PostListREST.as_view(), name="post_list_rest"),
    url(r'^posts/(?P<pk>\d+)/$', PostDetailREST.as_view(), name="post_detail_rest"),
)

#urlpatterns += patterns("blog.view",
#	url(r'^$', "post_list", ),
