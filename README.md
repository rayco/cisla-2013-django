# ============================================================================
# Cursos de Introducción al Software Libre para Alumnos (CISLA) - Mayo 2013
#	Oficina de Software Libre de la Universidad de La Laguna (OSL-ULL)
#							http://osl.ull.es
# ============================================================================
#
#				Desarrollo de aplicaciones web con Django
#
# ============================================================================
#
# Autor: Rayco Abad Martín <rayco.abad@gmail.com>
#
# ============================================================================
#
#					DESARROLLO DE UN BLOG CON DJANGO 1.5
#
# ============================================================================
#
#	Las funcionalidades que se han añadido al proyecto base son: 
#
#		- Completada la navegabilidad enlanzando las vistas de creación,
#			modificación y borrado
#
#		- Añadida API REST para los Posts (django-rest-framework)
#
#		- Añadido editor WYSIWYG (django-tinymce)	
#
#		- Integración de comentarios (django-disqus)
#
#		- Mejora del aspecto visual (bootstrap)
#
#		- Añadida autenticación de usuarios (django-social-auth)
#
# ============================================================================
